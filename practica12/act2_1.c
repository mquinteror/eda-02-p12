#include<stdio.h>
#include<omp.h>

int main()
{
	int sum=0;
	#pragma omp parallel num_threads(4) reduction (+:sum)
	{
		sum=sum+10;
	}
	printf("\nLa variable sum tiene %d\n\n", sum);
}

/*
que valor se imprime y porque?
la variable sum tiene el valor de 40, esto se debe a que ahora 
que especificamos el numero de hilos, y por la clausula reduction que esta tomara el valor de la variable inicializada anteriormente sobre esos datos. 
*/
