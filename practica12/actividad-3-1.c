//actividad 3.1
#include<stdio.h>
#include<omp.h>
void main()
{
 	#pragma omp parallel num_threads(4)
 	{
 		#pragma omp sections
 		{
 			#pragma omp section
 			{
 				printf ("\n El hilo %d esta en la region 1 ",omp_get_thread_num());
 			}
 		#pragma omp section
 		{
			printf ("\n El hilo %d esta en la region 2",omp_get_thread_num());
 		}
 		#pragma omp section
 		{
 			printf ("\n El hilo %d esta en la region 3\n\n", omp_get_thread_num());
 		}
 	   }
 	}
}


/*
que se visualiza a la salida y por que?

nos muestra el numero de hilo en la region que se le esta asignando
esto se debe a que se hizo en forma de rama paralela


al ejecutar varias veces el programa. �se obtiene la misma salida? porque?
en la tercer compilacion se pudo notar el cambio y esto se debe a que hubo un cambio en el orden de las iteracion en forma paralela. 


*/
