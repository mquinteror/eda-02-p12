/*Activiadad 1.1*/
#include<stdio.h>
#include<omp.h>

int main (void){
  int sum=0;
  #pragma omp parallel num_threads(4) reduction (+:sum)
  {
    sum = sum + 10;
  }
  printf("la variable sum tiene [%d]\n", sum);

  return 0;
}
