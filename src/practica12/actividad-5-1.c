//actividad 5
#include <stdio.h>
int main( ) 
{
 	int a[5], i;
 	#pragma omp parallel
 	{
 		#pragma omp for
 			for (i = 0; i < 5; i++)
 			a[i] = i * i;
 		#pragma omp master
 			for (i = 0; i < 5; i++)
 			printf("a[%d] = %d\n", i, a[i]);
 		//#pragma omp barrier

 		#pragma omp for
 			for (i = 0; i < 5; i++)
 		a[i] += i;
 	}
} 

/*
que sucede si se quita la barrera?


si en lugar de utilizar el constructor master se usa single?. que otros cambios se tienen que hacer en el codigo?
*/
