#include<stdio.h>
#include<omp.h>

int main()
{
	int i=0;
	#pragma omp parallel
	{
		#pragma omp critical
		{
			i++;
			printf("\n El hilo %d modifica i a: %d\n", omp_get_thread_num(),i);
		}
	}
}

/*que resultados se vieron? 
nos muestra el numero del hilo que esta modificando a su vez a i con su respectivo valor 
cabe mencionar que en la primer compilada, solo nos mostraba el hilo 0 que modifica i a= 1
y en la segunda compilación nos marco otro hilo.

*/
