//actividad 3.1
#include<stdio.h>
#include<omp.h>
void main()
{
#pragma omp parallel num_threads(4)
  {
#pragma omp sections
    {
#pragma omp section
      {
	printf ("\n El hilo %d esta en la region 1 ",omp_get_thread_num());
      }
#pragma omp section
      {
	printf ("\n El hilo %d esta en la region 2",omp_get_thread_num());
      }
#pragma omp section
      {
	printf ("\n El hilo %d esta en la region 3\n\n", omp_get_thread_num());
      }
    }
  }
}
