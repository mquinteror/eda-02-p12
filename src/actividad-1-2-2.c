/*Actividad 1.2 Version paralela*/
#include<stdio.h>
#include<omp.h>
#include<stdlib.h>
/* programa que encuentra el maximo elementto en un arreglo
   unidimensional de enteror
*/

// definicion de la funcion
int buscar_maximo(int *arreglo, int n){

  int max, i;
  max=arreglo[0];
  #pragma omp parallel
  {
  #pragma omp for private(max)
    for ( i=1; i<n; i++ ){
      if(arreglo[i] > max){
	#pragma omp critical
	max=arreglo[i];
      }
    }
    // el valor encontrado es:
  }
  return max;
}

// uso de la funcion

int main (void){
  
  int *arreglo, n, i;
  printf("se hara n = 30, y el arreglo sera rellenado con elementos de 2 en 2\n");
  n = 30;
  arreglo = (int*)malloc(sizeof(int)*n);
  
  // llenado del arreglo con el doble del idice
  for (i=0; i<n; i++){
    arreglo[i] = i*2;
  }

  // uso de la funcion
  int maximo = buscar_maximo(arreglo, n);
  printf("el numero maximo contenido en n es: [%d]\n", maximo);
}
