/*Activiadad 2.2 version en serie*/
#include<stdio.h>
#include<omp.h>
#include<stdlib.h>

//funcion del producto punto de dos vectores

double producto_punto(double *a, double *b, int n){
  double resultado = 0;
  int i;
  for ( i=0; i< n; i++ ){
    resultado = resultado + ( a[i]*b[i] );
  }
  return resultado;
}

int main (void){
  // creacion de un arrego
  printf("se crearan 2 arreglos de 1o elementos con los numeros del 0 al 9\n");

  int i;
  int n = 10;
  double *arreglo1, *arreglo2;
  arreglo1=(double*)malloc(sizeof(double)*n);
  arreglo2=(double*)malloc(sizeof(double)*n);
  for (i=0; i<n; i++){
    arreglo1[i]=i;
    arreglo2[i]=i;
  }

  // uso de la funcion
  double pp = producto_punto(arreglo1, arreglo2, n);

  printf("el producto punto es: [%f]\n", pp);

  return 0;
}
